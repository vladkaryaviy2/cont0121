<?php

namespace App\Http\Controllers\Offer;

use App\Events\OfferAccepted;
use App\Inquiry;
use App\Offer;
use App\Repositories\InquiryRepository;

class Controller extends \App\Http\Controllers\Controller
{
    public function accept(Offer $offer, $inquiryID)
    {
        $inquiry = InquiryRepository::getInquiryById($inquiryID);

        // Check weather inquiry is available and is same as offer has
        if ( ! $inquiry || $inquiry->id != $offer->inquiry->id)
        {
            return response()->json([
                'message' => 'Susisiekimas negalimas!',
            ], 404);
        }

        if ($offer->accepted)
        {
            return response()->json([
                'message' => 'Jūs jau susisiekėte su šiuo brokeriu, laukite atsakymo.',
            ], 401);
        }

        if ( ! $offer->inquiry->active)
        {
            return response()->json([
                'message' => 'Vienas iš brokerių apmokėjo užklausa, laukite atsakymo.',
            ], 401);
        }
        
        
        $offer->accept();

        $ptypeId = $offer->inquiry->property_type[0]->id;
        $pproId = $offer->inquiry->properties[0]->id;
        $pservID = $offer->inquiry->service->id;
        
        $chain = \App\ChainPropertyType::select('chain_service.*')->where('property_type_id',$ptypeId)
            ->join('chain_properties','chain_properties.chain_property_type_id', 'chain_property_type.id')
            ->where("chain_properties.property_id",$pproId)
            ->join('chain_service','chain_service.chain_property_id', 'chain_properties.id')
            ->where('chain_service.service_id',$pservID)
            ->first();
        if($chain && $chain->provider_fee == 0){

            $order = \App\Order::create([
                'amount' => 0,
                'user_id' => \Auth::user()->id,
                'token' => \Str::random(40),
                'model_type' => 'offer',
                'model_id' => $offer->id,
                'transaction_id' => 0,
                'payment_status' => \App\Order::PAYMENT_COMPLETED,
                'payment_method' => 'none',
            ]);

            $order->products()->create([
                'name' => $offer->getProductName(),
                'amount' => $offer->getProductServicePrice(false),
            ]);

            $offer->afterProductCreated($order);
            $offer->completed($order);
        }
        event(new OfferAccepted($offer, $inquiry));

        return response()->json([
            'message' => 'Jūsų kontaktai išsiųsti paslaugos teikėjui'
        ], 201);
    }
}
