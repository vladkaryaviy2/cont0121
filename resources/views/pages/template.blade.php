@extends('layouts.templates.full-width')

@section('pageTitle', $title)

@section('content')

    <h2>{{ $title }}</h2>

    {!! $content !!}

@endsection
