<tabs
    :tabs="{{ collect([
        [
            'text' => 'Registracija Užsakovams',
            'link' => route('register.regular'),
            'disabled' => false
        ],
        [
            'text' => 'Registracija NT specialistams',
            'link' => route('register.broker'),
            'disabled' => false,
        ]
        ]) }}"
    current-link="{{ Request::fullUrl() }}"
></tabs>
