@extends('layouts.templates.contained')

@section('content')
    
    <div class="text-center font-weight-bold mb-3">
        <a href="{{ route('login') }}">Esate narys? Prisijungti</a>
    </div>
    <div class="register register--broker">
        @include('auth.partials.tabs')
        <iframe class="d-block mx-auto mb-5" width="610" height="500" src="https://www.youtube.com/embed/V2TNpThEc4o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <!-- <h2>Vartotojo registracija</h2> -->

        <user-form
            agreement-link="{{ env('TEST_DATA') ? 'test' : setting('site.agreement_url') }}"
            post-route="{{ route('register.regular') }}"
        ></user-form>
    </div>
@endsection
