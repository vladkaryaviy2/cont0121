@extends('layouts.templates.contained')

@section('content')

    <div class="register register--broker">
        <h2>Atstatyti slaptažodį</h2>
        <reset-password-form></reset-password-form>
    </div>

@endsection
