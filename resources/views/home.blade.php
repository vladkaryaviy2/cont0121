@extends('layouts.templates.full-width')

@section('content')

    @include('sections.getanoffer')
    @include('sections.videos')
    @include('sections.top-companies')
    @include('sections.find-company')

@endsection
